import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HtModule} from "ht-angular";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HtModule.forRoot({
      token: "sk_74e451161b74385aa105c68861cd2d87ebd9a2c3",
      mapType: "google"
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
